import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import Login from "./modules/auth/components/Login";
import Layout from "./modules/layout/layout";
import { useAuth } from "./modules/auth/components/Auth";
import "bootstrap/dist/css/bootstrap.min.css";
import UserList from "./modules/users/components/userList";
import Dashboard from "./modules/dashboard/component/dashboard";
import RoleList from "./modules/roles/components/RoleList";
import AddFormRole from "./modules/roles/components/AddFormRole";
import UpDateRole from "./modules/roles/components/UpDateRole";
import RoomType from "./modules/roomtype/components/roomType";
import RoomManagement from "./modules/roommanagement/components/roomManagement";

const App = () => {
  const { auth } = useAuth();

  return (
    <BrowserRouter>
      <Routes>
        {auth !== null ? (
          <>
            <Route path="/" element={<Layout />}>
              {/* <Route path="/" element={<MainLayout/>}/> */}
              <Route path="userList" element={<UserList />} />
              {/* <Route path="addformRole" element={<AddFormRole />} /> */}
              <Route path="roleList" element={<RoleList />} />
              <Route path="updataRole/:id" element={<UpDateRole />} />
              <Route path="dashboard" element={<Dashboard />} />
              <Route path="roomType" element={<RoomType />} />
              <Route path="roomManagement" element={<RoomManagement />} />
            </Route>

            <Route path="/*" element={<Navigate to="/" />}></Route>
          </>
        ) : (
          <>
            <Route index path="auth/login" element={<Login />}></Route>
            <Route path="*" element={<Navigate to="/auth/login" />}></Route>
          </>
        )}
      </Routes>
    </BrowserRouter>
  );
};

export default App;
