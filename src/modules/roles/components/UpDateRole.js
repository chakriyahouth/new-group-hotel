import React, { useState } from "react";
import { Col, Row } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import { useNavigate, useParams } from "react-router";
import { useRoles } from "../core/action";
import { useEffect } from "react";
import { useSelector } from "react-redux";

const UpDateRole = () => {
  const id = useParams().id;
  const { listRoleById } = useSelector((state) => state.roles);
  const [show, setShow] = useState(true);
  const navigate = useNavigate();
  const { onUpdateRoles, showRoleById } = useRoles();
  const [updateRole, setNewHotel] = useState({
    name: "",
    code: "",
  });

  console.log(id);
  if (show === false) {
    navigate("/roleList");
  }
  const handleClose = () => {
    // e.preventDefault();
    setShow(false);
  };
  useEffect(() => {
    showRoleById(id);
  }, [id]);

  const handleSave = (e) => {
    e.preventDefault();
    let isConfirm = window.confirm("Are you sure you want to Edit?");
    if (isConfirm === true) {
      onUpdateRoles({ ...updateRole });
    }
    setNewHotel({
      name: "",
      code: "",
    });
    setShow(false);
  };
  console.log(listRoleById);
  return (
    <>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title className={"fs-6 fw-bold"}>Create New</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Row>
              <Row>
                <Col xs={12} md={8}>
                  <Form.Group
                    className="mb-3"
                    controlId="exampleForm.ControlInput1"
                  >
                    <Form.Label> Name : </Form.Label>
                    <Form.Control
                      type="text"
                      defaultValue={listRoleById.role.name}
                      onChange={(e) =>
                        setNewHotel({ ...updateRole, name: e.target.value })
                      }
                      autoFocus
                    />
                  </Form.Group>
                </Col>
              </Row>
              <Row>
                <Col xs={12} md={8}>
                  <Form.Group
                    className="mb-3"
                    controlId="exampleForm.ControlTextarea1"
                  >
                    <Form.Label>Code : </Form.Label>
                    <Form.Control
                      type="text"
                      defaultValue={listRoleById.role.code}
                      onChange={(e) =>
                        setNewHotel({ ...updateRole, code: e.target.value })
                      }
                    />
                  </Form.Group>
                </Col>
              </Row>
            </Row>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={(e) => handleSave(e)}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};
export default UpDateRole;
