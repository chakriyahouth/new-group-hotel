import React from "react";
import { Space, Table } from "antd";
import { useEffect } from "react";
import { useNavigate } from "react-router";

// import AddFormRole from "./AddFormRole";
import { useSelector } from "react-redux";
import { MdDelete } from "react-icons/md";
import { CiEdit } from "react-icons/ci";
import { IoMdAddCircleOutline } from "react-icons/io";
import { useRoles } from "../core/action";
import { updateList } from "../core/reducer";



const RoleList = () => {
  const { list } = useSelector((state) => state.roles);
  const { fetchRoles, onDeleteRole } = useRoles();
  // const { AddFormRole } = AddFormRole();
  const navigate = useNavigate();

  useEffect(() => {

    fetchRoles();

  }, []);
console.log(list);
  const { Column } = Table;
  return (
    <>
      <div className=" d-flex  justify-content-end ​" >
        <button
          className="p-0 border-0 bg-transparent   "
          onClick={() => navigate("/addformRole")}
        >
          <IoMdAddCircleOutline  />
        </button>
      </div>{" "}
      <Table dataSource={list}>
        //jab data tam dataIndex
        <Column title={"id"} dataIndex="id" key="id"  />
        <Column title="Name" dataIndex="name" key="name" />
        <Column title="Code" dataIndex="code" key="code" />
        <Column
          title="Action"
          key="action"
          render={(record) => (
            <Space size="middle">
              <button
                className="p-0 border-0 bg-transparent"
                onClick={() => navigate(`/updataRole/${record.id}`)}
              >
                <CiEdit className="fs-5  " style={{ color: "blue" }} />
              </button>
              <button
                className="p-0 border-0 bg-transparent"
                onClick={() => onDeleteRole(record.id)}
              >
                <MdDelete className=" fs-5  " style={{ color: "red" }} />
              </button>
            </Space>
            // <Space size="middle">
            //   <button
            //   className="p-0 border-0 bg-transparent"
            //     onClick={() => navigate(`/updataRole/${record.id}`)}
            //     >
                
            //   <CiEdit />
            //   </button>
              
            //   <button
            //     className="p-0 border-0 bg-transparent"
            //     onClick={() => onDeleteRole(record.id)}
            //   >
            //     <MdDelete />
            //   </button>
            // </Space>
          )}
        />
      </Table>
    </>
  );
};

export default RoleList;
