import axios from "axios";
import { getAuth } from "../../auth/components/AuthHelper";

const getRoles = () => {
  return axios.get("api/roles");
};

const deleteRoles = (id) => {
  return axios.delete(`api/roles/${id}`);
};
const addRoles = ({ ...data }) => {
  return axios.post(
    `api/roles`,
    {
      ...data,
    },
    {
      headers: {
        Authorization: `Bearer ${getAuth()}`,
      },
    }
  );
};
const getRoleById = (id) => {
  return axios.get(`api/roles/${id}`, {
    headers: {
      Authorization: `Bearer ${getAuth()}`,
    },
  });
};
// const updatePermission = (id, updatePermission) => {
//   return axios.put(
//     `api/roles`,
//     {
//       role_id: id,
//       permissions: updatePermission,
//     },
//     {
//       headers: {
//         authorization: `Bearer ${getAuth()}`,
//       },
//     }
//   );
// };

const updateRoles = (id, name) => {
  return axios.put(
    `api/roles/${id}`,
    {
      name: name,
    },
    {
      headers: {
        Authorization: `Bearer ${getAuth()}`,
      },
    }
  );
};

export { getRoles, deleteRoles, addRoles, updateRoles, getRoleById };
