import { createSlice } from "@reduxjs/toolkit";



const roleSlice = createSlice({
    name: "roles",
    initialState: {
        loading: false,
        list: [],
        listRoleById:{},
    },
    reducers: {
        setList: (state, action) => {
            state.list = action.payload;
        },
        addList: (state, action) => {
            state.listRoles.push(action.payload);
        },
        updateList: (state, action) => {
            state.listRoles.push(action.payload);
        },
        RoleById: (state,action) =>  {
            state.listRoleById = action.payload;
        },
        }
    
});

export const {setList,addList,updateList,RoleById}  = roleSlice.actions;
export default roleSlice.reducer;