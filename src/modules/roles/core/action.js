import { useDispatch } from "react-redux";
import Swal from "sweetalert2";
import {
  addRoles,
  deleteRoles,
  getRoles,
  getRoleById,
} from "./request";
import {
  addList,
  setList,
  updateList,
  RoleById,
} from "./reducer";

const useRoles = () => {
  const dispatch = useDispatch();

  const fetchRoles = async () => {
    try {
      await getRoles().then((response) => {
        // console.log(response.data.data);
        dispatch(setList(response.data.data));
      });
    } catch (error) {
      console.log(error);
    }
  };
  const Rolesadd = async ({ ...addRole }) => {
    try {
      const response = await addRoles({ ...addRole });
      // console.log(response);
      dispatch(addList(response));
    } catch (error) {
      console.error("Internal Server Error:", error);
    }
  };
  const onUpdateRoles = async ({ ...updateRole }) => {
    try {
      const result = await updateList({ ...updateRole });
      dispatch(updateList(result));
    } catch (error) {}
  };

  const showRoleById = (id) => {
    getRoleById(id).then((res) => {
      dispatch(RoleById(res.data.data));
    });
  };

  // const getUpdatePermission = (data) => {
  //   try {
  //     updatePermission(data).then((res) => {
  //       test.success(`Role update ${res.data.message} `, {
  //         position: "top-center",
  //       });
  //       dispatch(setUpdatePermission(res.data.data));
  //     });
  //   } catch (error) {
  //     console.error("Failed to fetch roles:", error);
  //     throw error;
  //   }
  // };

  const onDeleteRole = (id) => {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger",
      },
      buttonsStyling: false,
    });
    swalWithBootstrapButtons
      .fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          deleteRoles(id)
            .then(() => {
              swalWithBootstrapButtons.fire({
                title: "Deleted!",
                text: "Your User has been deleted.",
                icon: "success",
              });
              fetchRoles();
            })
            .catch((err) => {
              console.log(err);
              swalWithBootstrapButtons.fire({
                title: "Error!",
                text: err.response.data.message,
                icon: "error",
              });
            });
        }
      });
  };

  return {
    fetchRoles,
    onDeleteRole,
    Rolesadd,
    onUpdateRoles,
    showRoleById,
  };
};

export { useRoles };
