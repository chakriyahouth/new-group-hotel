import React from "react";

import { Space, Table } from "antd";
import { useEffect, useState } from "react";
import { useRoomType } from "../core/action";
import { useSelector } from "react-redux";
import { MdDelete } from "react-icons/md";
import { CiEdit } from "react-icons/ci";
import { Button, Flex } from "antd";
import FormCreate from "./FormCreate";

const RoomManagement = () => {
  const { fetchRoomType, onDelete } = useRoomType();
  const { list } = useSelector((state) => state.roomtype);
  const [add, toggleAdd] = useState(false);
  const [record, setRecord] = useState();

  useEffect(() => {
    fetchRoomType();
  }, []);

  useEffect(() => {
    !add && setRecord({});
  }, [add]);

  const { Column } = Table;

  return (
    <>
      <div
        className="d-flex justify-content-end"
        onClick={() => {
          toggleAdd(!add);
        }}
      >
        <Button type="primary">Add Room</Button>
      </div>

      {add && <FormCreate record={record} toggleAdd={toggleAdd} />}

      <Table dataSource={list}>
        {" "}
        //jab data tam dataIndex
        <Column title={"id"} dataIndex="id" key="id" />
        <Column title={"Title"} dataIndex="title" key="title" />
        <Column title={"Subtitle"} dataIndex="subTitle" key="subTitle" />
        <Column
          title={"Description"}
          dataIndex="description"
          key="description"
        />
        <Column title={"Price"} dataIndex="price" key="price" />
        <Column title={"Bed"} dataIndex="bed" key="bed  " />
        <Column title={"Amenity"} dataIndex="amenity" key="amenity " />
        <Column
          title="Action"
          key="action"
          render={(record) => (
            <Space size="middle">
              <button
                className="p-0 border-0 bg-transparent"
                onClick={() => {
                  toggleAdd(true);
                  setRecord(record);
                }}
              >
                <CiEdit className="fs-5  " style={{ color: "blue" }} />
              </button>
              <button
                className="p-0 border-0 bg-transparent"
                onClick={() => onDelete(record.id)}
              >
                <MdDelete className=" fs-5  " style={{ color: "red" }} />
              </button>
            </Space>
          )}
        />
      </Table>
    </>
  );
};

export default RoomManagement;
