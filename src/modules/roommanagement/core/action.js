import { useDispatch } from "react-redux";
import {
  createRoom,
  deleteRoomType,
  deleteUser,
  getRoomType,
  updateRoomType,
} from "./request";
import { setList } from "./reducer";
import Swal from "sweetalert2";

const useRoomType = () => {
  const dispatch = useDispatch();

  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: "btn btn-success",
      cancelButton: "btn btn-danger",
    },
    buttonsStyling: false,
  });

  // const fetchUsers = () => {
  //     return getUsers().then((users) => {
  //         // dispatch(setUsers(users))
  //     })
  // }

  const onCreate = (createData) => {
    return createRoom(createData)
      .then(() => {
        swalWithBootstrapButtons.fire({
          title: "Created!",
          text: "Your room has been created",
          icon: "success",
        });
        fetchRoomType();
      })
      .catch((err) => {
        swalWithBootstrapButtons.fire({
          title: "Error!",
          text: err.response.data.message,
          icon: "error",
        });
      });
  };

  const onUpdate = (id, payload) => {
    return updateRoomType(id, payload)
      .then(() => {
        swalWithBootstrapButtons.fire({
          title: "Created!",
          text: "Your room has been created",
          icon: "success",
        });
        fetchRoomType();
      })
      .catch((err) => {
        swalWithBootstrapButtons.fire({
          title: "Error!",
          text: err.response.data.message,
          icon: "error",
        });
      });
  };

  const fetchRoomType = () => {
    getRoomType().then((res) => {
      res.data.data.map((e) => {
        e.kid = e.children;
        delete e.children;
        return e;
      });
      dispatch(setList(res.data.data));
    });
  };

  const onDelete = (id) => {
    swalWithBootstrapButtons
      .fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          deleteRoomType(id)
            .then(() => {
              swalWithBootstrapButtons.fire({
                title: "Deleted!",
                text: "Your User has been deleted.",
                icon: "success",
              });
              fetchRoomType();
            })
            .catch((err) => {
              console.log(err);
              swalWithBootstrapButtons.fire({
                title: "Error!",
                text: err.response.data.message,
                icon: "error",
              });
            });
        }
      });
  };

  return { fetchRoomType, onDelete, onCreate, onUpdate };
};

export { useRoomType };
