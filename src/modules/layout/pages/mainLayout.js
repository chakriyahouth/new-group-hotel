import React, { useState } from "react";
import { MenuFoldOutlined, MenuUnfoldOutlined } from "@ant-design/icons";
import { Layout, Menu, Button, theme } from "antd";
// import { AiOutlineDashboard, } from "react-icons/ai";
import { GiNotebook } from "react-icons/gi";
import { BiSolidCategoryAlt } from "react-icons/bi";
import { IoIosPricetag } from "react-icons/io";
import { FaUser } from "react-icons/fa";
import { CiLogout } from "react-icons/ci";
import { IoLogOut } from "react-icons/io5";
import { FaSearchDollar } from "react-icons/fa";
import { WiHorizonAlt } from "react-icons/wi";
import { Outlet, useNavigate } from "react-router-dom";
import { useAuth } from "../../auth/components/Auth";
import Stack from "react-bootstrap/Stack";

const { Header, Sider, Content } = Layout;
// import userList from "../../users/components/userList";
const MainLayout = () => {
  const { removeAuth } = useAuth();
  const navigate = useNavigate();
  const [collapsed, setCollapsed] = useState(false);
  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken();
  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="demo-logo-vertical" />
        <Menu
          theme="dark"
          mode="inline"
          defaultSelectedKeys={[""]}
          onClick={({ key }) => {
            if (key === "signout") {
            } else {
              navigate(key);
            }
          }}
          items={[
            {
              label: "Hotel System",
            },
            {
              key: "roomType",
              icon: <GiNotebook />,
              label: "Room Type",
            },
            {
              key: "roomManagement",
              icon: <GiNotebook />,
              label: "Room Management",
            },

            {
              key: "userList",
              icon: <FaUser />,
              label: "User Management",
            },
            {
              key: "roleList",
              icon: <IoIosPricetag />,
              label: "Roles ",
            },
            {
              icon: <CiLogout />,
              label: "Logout",
              onClick: () => removeAuth(),
            },
          ]}
        />
      </Sider>
      <Layout>
        <Header
          style={{
            padding: 1,
            background: colorBgContainer,
          }}
        >
          <Stack direction="horizontal" gap={3}>
            <div className="p-2 ms-auto">
              <FaSearchDollar />
            </div>
            <div className="p-2">
              <WiHorizonAlt />
            </div>
            <div>
              <Button onClick={() => removeAuth()}>
                <IoLogOut />
              </Button>
            </div>
          </Stack>
          <Button
            type="text"
            icon={collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
            onClick={() => setCollapsed(!collapsed)}
            style={{
              fontSize: "16px",
              width: 64,
              height: 64,
            }}
          />
        </Header>
        <Content
          style={{
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
            background: colorBgContainer,
            borderRadius: borderRadiusLG,
          }}
        >
          <Outlet />
        </Content>
      </Layout>
    </Layout>
  );
};

export default MainLayout;
