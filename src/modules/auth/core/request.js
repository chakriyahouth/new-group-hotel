import axios from "axios";
import { getAuth } from "../components/AuthHelper";

const login = (username, pass)=>{
    return axios.post("/api/auth/login",{
        username: username,
        password: pass,
    })
}

const getUser = () =>{
    return axios.get("/auth/me",{
        headers:{
            "Authorization":`Bearer ${getAuth()}`
        }
    });
}

const getRoom = () => {
    return axios.get("/v1.0.0/room",{
        headers:{
            "Authorization":`Bearer ${getAuth()}`
        }
    });
}


const getUserList = () =>{
    return axios.get("api/users",{
        headers:{
            "Authorization":`Bearer ${getAuth()}`
        }
    });
}

export{login, getUser, getRoom, getUserList}