


const Resetpassword = () => {
//hello
    return <div className="align-content-center p-5" >
        <div className="col-md-4 offset-md-4" style={{backgroundColor: "#F2D7D5"}}>
            <p className="fst-normal text-center">Reset Account Password</p>
            <div className="fst-normal text-center">Enter A new Password</div>

            <form>
                <div className="mb-3">
                    <label htmlFor="exampleInputPassword1" className="form-label">Old Password</label>
                    <input type="password" className="form-control" id="exampleInputPassword1"/>
                </div>
                <div className="mb-3">
                    <label htmlFor="exampleInputPassword1" className="form-label">New Password</label>
                    <input type="password" className="form-control" id="exampleInputPassword2"/>
                </div>
                <div className="mb-3">
                    <label htmlFor="exampleInputPassword1" className="form-label">Confirm Password</label>
                    <input type="password" className="form-control" id="exampleInputPassword3"/>
                </div>
                <button type="submit" className="btn btn-primary">Reset Password</button>
            </form>
        </div>
    </div>
}

export default Resetpassword;