import { useContext } from "react";
import { createContext, useState} from "react";
import * as authHelper from './AuthHelper'

const AuthContext  = createContext({
    auth: null,
    saveAuth: function(){

    },
    removeAuth: function(){

    },
    user: null,
    setUser: function(){

    },
    
});

    const useAuth = () => useContext(AuthContext);
        const AuthProvider = ({children}) => {

        
        const [auth, setAuth] = useState(authHelper.getAuth);
        const [user, setUser] = useState();
    
        const saveAuth = (auth) =>{
            setAuth(auth)
            authHelper.setAuth(auth)
        }
            const removeAuth = () =>{
                setAuth(null)
                authHelper.removeAuth()
            }
        
        return <AuthContext.Provider value ={{
            auth,
            saveAuth,
            user,
            setUser,
            removeAuth,

        }}>
            {children}
        </AuthContext.Provider>
        }
    
    export {useAuth, AuthProvider}