import axios from "axios";

const setAuth = (value) =>
    localStorage.setItem("token", value);

const getAuth = () =>
    localStorage.getItem("token");
const removeAuth = () =>
    localStorage.removeItem("token");

const setUpAxios = () => {
    axios.defaults.baseURL = "http://13.214.207.172:6002/";


    axios.interceptors.request.use(function (config) {
        // Do something before request is sent
        config.headers.Authorization= getAuth() != null ? `Bearer ${getAuth()}`:"Basic aG90ZWw6aG90ZWxAMTIz"
        return config;
    }, function (error) {
        // Do something with request error
        return Promise.reject(error);
    });
}
export { setAuth, getAuth, setUpAxios ,removeAuth}