import React, { useState } from "react";
import "../../../App.css";
import { Button, Form, Input, InputNumber, Mentions } from "antd";
// import {onCreate} from "../core/action";
import { useUsers } from "../core/action";
import { IoIosCloseCircleOutline } from "react-icons/io";

const formItemLayout = {
    labelCol: {
        xs: {
            span: 24,
        },
        sm: {
            span: 6,
        },
    },
    wrapperCol: {
        xs: {
            span: 24,
        },
        sm: {
            span: 14,
        },
    },
};
// const {fetchUsers} = useUsers();
const FormCreate = ({ toggleAdd, record = {} }) => {
    const { onCreate, onUpdate } = useUsers();
    const [createData, setCreateData] = useState({
        username: "",
        // sub_title: "",
        name: "",
        phone: "",
        email: "",
        // children: "",
        password: "",
        address: "",
        dateOfBirth: "",
        status:'',
        roleId:'',
        bio:"",
        ...record,
    });

    const {
        username,
        name,
        phone,
        email,
        password,
        address,
        dateOfBirth,
        status,
        roleId,
        bio,
    } = createData;

    console.log(record);

    const handleInputChange = (fieldName, value) => {
        setCreateData((prevState) => ({
            ...prevState,
            [fieldName]: value,
        }));
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        if (record.id != null) {
            onUpdate(record.id, createData).then(() => toggleAdd(false));
        } else {
            onCreate(createData).then(() => {
                toggleAdd(false);
            });
        }
    };

    return (
        <div
            className="hello position-fixed w-50 shadow-background pt-0 d-flex justify-content-center flex-column text-white "
            style={{ zIndex: 1000, marginLeft: "220px", background: "white" }}
        >
            <IoIosCloseCircleOutline
                onClick={() => toggleAdd(false)}
                className="position-absolute top-0 left-0 text-danger fs-4 m-2 "
            />
            <h3 className="text-center fw-bold text-dark">Add User Form</h3>
            <div className="hello w-100 ">
                <Form
                    {...formItemLayout}
                    variant="filled"
                    style={{
                        maxWidth: 700,
                        color: "white",
                    }}
                >
                    <Form.Item
                        label="UserName"
                        rules={[
                            {
                                required: true,
                                message: "Please input!",
                            },
                        ]}
                        style={{ color: "white" }}
                    >
                        <Input
                            defaultValue={username}
                            onChange={(e) => {
                                handleInputChange("username", e.target.value);
                            }}
                        />
                    </Form.Item>
                    {/*<Form.Item*/}
                    {/*    label="subtitle"*/}
                    {/*    name="subtitle"*/}
                    {/*    rules={[*/}
                    {/*        {*/}
                    {/*            required: true,*/}
                    {/*            message: "Please input!",*/}
                    {/*        },*/}
                    {/*    ]}*/}
                    {/*>*/}
                    {/*    <Input*/}
                    {/*        defaultValue={sub_title}*/}
                    {/*        onChange={(e) => handleInputChange("sub_title", e.target.value)}*/}
                    {/*    />*/}
                    {/*</Form.Item>*/}
                    <Form.Item
                        label="Name"
                        name="TextArea"
                        rules={[
                            {
                                required: true,
                                message: "Please input!",
                            },
                        ]}
                    >
                        <Input.TextArea
                            defaultValue={name}
                            onChange={(e) => handleInputChange("name", e.target.value)}
                        />
                    </Form.Item>
                    <Form.Item
                        label="Phone"
                        name="phone"
                        rules={[
                            {
                                required: true,
                                message: "Please input!",
                            },
                        ]}
                    >
                        <InputNumber
                            style={{
                                width: "100%",
                            }}
                            defaultValue={phone}
                            onChange={(value) => handleInputChange("phone", value.toString())}
                        />
                    </Form.Item>
                    <Form.Item
                        label="Email"
                        name="email"
                        rules={[
                            {
                                required: true,
                                message: "Please input!",
                            },
                        ]}
                    >
                        <InputNumber
                            style={{
                                width: "100%",
                            }}
                            defaultValue={email}
                            onChange={(value) => handleInputChange("email", value.toString())}
                        />
                    </Form.Item>
                    <Form.Item
                        label="Password"
                        name="password"
                        rules={[
                            {
                                required: true,
                                message: "Please input!",
                            },
                        ]}
                    >
                        <InputNumber
                            style={{
                                width: "100%",
                            }}
                            defaultValue={password}
                            onChange={(value) => handleInputChange("password", value.toString())}
                        />
                    </Form.Item>
                    <Form.Item
                        label="Address"
                        name="address"
                        rules={[
                            {
                                required: true,
                                message: "Please input!",
                            },
                        ]}
                    >
                        <InputNumber
                            style={{
                                width: "100%",
                            }}
                            defaultValue={address}
                            onChange={(value) =>
                                handleInputChange("address", value.toString())
                            }
                        />
                    </Form.Item>
                    <Form.Item
                        label="dateOfBirth"
                        name="dateOfBirth"
                        rules={[
                            {
                                required: true,
                                message: "Please input!",
                            },
                        ]}
                    >
                        <Mentions
                            defaultValue={dateOfBirth}
                            onChange={(value) => handleInputChange("dateOfBirth", parseInt(value))}
                        />
                    </Form.Item>
                    <Form.Item
                        label="status"
                        name="status"
                        rules={[
                            {
                                required: true,
                                message: "Please input!",
                            },
                        ]}
                    >
                        <Mentions
                            defaultValue={status}
                            onChange={(value) =>
                                handleInputChange("status", value.toString())
                            }
                        />
                    </Form.Item>
                    <Form.Item
                        label="RoleId"
                        name="roleId"
                        rules={[
                            {
                                required: true,
                                message: "Please input!",
                            },
                        ]}
                    >
                        <InputNumber
                            style={{
                                width: "100%",
                            }}
                            defaultValue={roleId}
                            onChange={(value) => handleInputChange("roleId", value.toString())}
                        />
                    </Form.Item>
                    <Form.Item
                        label="Bio"
                        name="bio"
                        rules={[
                            {
                                required: true,
                                message: "Please input!",
                            },
                        ]}
                    >
                        <InputNumber
                            style={{
                                width: "100%",
                            }}
                            defaultValue={bio}
                            onChange={(value) => handleInputChange("bio", value.toString())}
                        />
                    </Form.Item>
                    <Form.Item
                        wrapperCol={{
                            offset: 6,
                            span: 16,
                        }}
                    >
                        <Button onClick={handleSubmit} type="primary" htmlType="submit">
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        </div>
    );
};

export default FormCreate;
