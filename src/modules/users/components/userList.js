import React, {useState} from 'react';
import UpdateUser from "./updateUser";
import { Space, Table } from 'antd';
import { useEffect } from 'react';
import { useUsers } from '../core/action';
import { useSelector } from 'react-redux';
import { MdDelete } from "react-icons/md";
import { CiEdit } from "react-icons/ci";
import { IoIosAddCircle } from "react-icons/io";
import {Button} from 'antd';
import FormCreateUser from "./FormCreateUser";
const UserList =() => {

  const { fetchUsers ,onDelete} = useUsers();
  const {list} = useSelector(state=>state.users)

      useEffect(() => {
        fetchUsers()
      }, [])

    const [add, toggleAdd] = useState(false);
    const [record, setRecord] = useState();
    useEffect(()=>{
        !add && setRecord({});
    }, [add]);


  const { Column } = Table;
  return (
      <>
      <div
          className="d-flex justify-content-end"
          onClick={() => {
              toggleAdd(!add);
          }}
      >
          <Button>
          <IoIosAddCircle />
          Add User
      </Button>
      </div>
    {add && <FormCreateUser record={record} toggleAdd={toggleAdd} />}



    <Table dataSource={list}> //jab data tam dataIndex

        <Column title={"id"} dataIndex="id" key="id" />
      <Column title="Name" dataIndex="name" key="name" />
      <Column title="Address" dataIndex="address" key="address" />
      <Column title="Status" dataIndex="status" key="status" />
        <Column title="Email" dataIndex="email" key="email" />
        <Column title="Phone" dataIndex="phone" key="phone" />
        <Column title="Bio" dataIndex="bio" key="bio" />
      <Column
        title="Action"
        key="action"
        render={(record) => (
            <Space size="middle">

                <Button className="p-0 border-0 bg-transparent" onClick={UpdateUser}>
                    <CiEdit/>
                </Button>
                <Button className="p-0 border-0 bg-transparent" onClick={() => onDelete(record.id)}>
                    <MdDelete/>
                </Button>
            </Space>
        )}
      />
    </Table>

      </>
  );
}

console.log(UpdateUser);
export default UserList;


