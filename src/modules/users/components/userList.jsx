import React from "react";
import { Space, Table } from "antd";
import { useEffect } from "react";
import { useUsers } from "../core/action";
import { useSelector } from "react-redux";
import { MdDelete } from "react-icons/md";
import { CiEdit } from "react-icons/ci";

const UserList = () => {
  const { fetchUsers, onDelete } = useUsers();
  const { list } = useSelector((state) => state.users);

  useEffect(() => {
    fetchUsers();
  }, []);

  const { Column } = Table;
  return (
    <Table dataSource={list}>
      {" "}
      //jab data tam dataIndex
      <Column title={"id"} dataIndex="id" key="id" />
      <Column title="Name" dataIndex="name" key="name" />
      <Column title="Address" dataIndex="address" key="address" />
      <Column title="Status" dataIndex="status" key="status" />
      <Column title="Email" dataIndex="email" key="email" />
      <Column title="Phone" dataIndex="phone" key="phone" />
      <Column title="Bio" dataIndex="bio" key="bio" />
      <Column
        title="Action"
        key="action"
        render={(record) => (
          <Space size="middle">
            <CiEdit className=" fs-5  " style={{ color: "blue" }} />
            <button
              className="p-0 border-0 bg-transparent"
              onClick={() => onDelete(record.id)}
            >
              <MdDelete className=" fs-5 bg-  " style={{ color: "red" }} />
            </button>
          </Space>
        )}
      />
    </Table>
  );
};

export default UserList;
