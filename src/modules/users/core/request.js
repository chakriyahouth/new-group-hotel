import axios from "axios"

// const getUsers = () =>{
//     return axios.get(`/api/users`);
// }
const onCreate = (payload)=>{
    return axios.post("api/users", payload);
}

const getUsers = ()=>{
    return axios.get("api/users");
}

const deleteUser = (id) =>{
    return axios.delete(`/api/users/${id}`);
}

const updateUser = (id) => {
  return axios.put(`/api/users/${id}`);
}

export{getUsers, deleteUser,updateUser, onCreate}