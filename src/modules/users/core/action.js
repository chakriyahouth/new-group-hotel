import { useDispatch } from "react-redux"
import {deleteUser, getUsers} from "./request"

import { setUsers } from "./reducer";
import Swal from "sweetalert2";



const useUsers = () => {

    const dispatch = useDispatch();

    // const fetchUsers = () => {
    //     return getUsers().then((users) => {
    //         // dispatch(setUsers(users))
    //     })
    // }

    const  swalWithBootstrapButtons = Swal.mixin({
        customClass:{
            confirmButton:"btn btn-success",
            cancelButton: "btn btn-danger",
        },
        buttonsStyling: false,
    });

     const fetchUsers = () => {
          getUsers().then((res) => {
              // const  format = res.data.data
              // format.forEach((user) =>{
              //     dispatch(setUsers(user))
              // })
              dispatch(setUsers( res.data.data))
          })
        }



    const onCreate = (createData) => {
        return createData(createData)
            .then(() => {
                swalWithBootstrapButtons.fire({
                    title: "Created!",
                    text: "Your room has been created",
                    icon: "Success",
                });
                fetchUsers();
            })
            .catch((err)=>{
                swalWithBootstrapButtons.fire({
                    title: "Error!",
                    text: err.response.data.message,
                    icon: "error",
                });
            });
    };
    const onDelete = (id) => {
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: "btn btn-success",
                cancelButton: "btn btn-danger"
            },
            buttonsStyling: false
        });
        swalWithBootstrapButtons.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
               deleteUser(id).then(()=>{
                   swalWithBootstrapButtons.fire({
                       title: "Deleted!",
                       text: "Your User has been deleted.",
                       icon: "success"
                   });
                   fetchUsers();
               }).catch((err)=>{
                   console.log(err)
                   swalWithBootstrapButtons.fire({
                       title: "Error!",
                       text: err.response.data.message,
                       icon: "error"
                   });
               })
            }
        });
    };

    return {onDelete, onCreate, fetchUsers}

};

export { useUsers }