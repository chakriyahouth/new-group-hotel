import { createSlice } from "@reduxjs/toolkit";

const roomTypeSlice = createSlice({
  name: "roomtype",
  initialState: {
    loading: false,
    list: [],
  },
  reducers: {
    setList: (state, action) => {
      state.list = action.payload;
    },
  },
});

export const { setList } = roomTypeSlice.actions;
export default roomTypeSlice.reducer;
