import axios from "axios";

const getRoomType = () => {
  return axios.get("api/v1.0.0/room-type");
};

const createRoom = (payload) => {
  return axios.post(`api/v1.0.0/room-type`, payload);
};

const deleteRoomType = (id) => {
  return axios.delete(`api/v1.0.0/room-type/${id}`);
};
const updateRoomType = (id, payload) => {
  return axios.put(`api/v1.0.0/room-type/${id}`, payload);
};
export { getRoomType, deleteRoomType, createRoom, updateRoomType };
