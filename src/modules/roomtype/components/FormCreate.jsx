import React, { useState } from "react";
import "../../../App.css";
import { Button, Form, Input, InputNumber, Mentions } from "antd";
import { createRoom } from "../core/request";
import { useRoomType } from "../core/action";
import { IoIosCloseCircleOutline } from "react-icons/io";

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 6,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 14,
    },
  },
};

const FormCreate = ({ toggleAdd, record = {} }) => {
  const { onCreate, onUpdate } = useRoomType();
  const [createData, setCreateData] = useState({
    title: "",
    sub_title: "",
    description: "",
    bed: "",
    adult: "",
    children: "",
    price: null,
    amenity: "",
    children: record.kid,
    sub_title: record.subTitle,
    ...record,
  });

  const {
    title,
    sub_title,
    description,
    bed,
    adult,
    children,
    price,
    amenity,
  } = createData;

  console.log(record);

  const handleInputChange = (fieldName, value) => {
    setCreateData((prevState) => ({
      ...prevState,
      [fieldName]: value,
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (record.id != null) {
      onUpdate(record.id, createData).then(() => toggleAdd(false));
    } else {
      onCreate(createData).then(() => {
        toggleAdd(false);
      });
    }
  };

  return (
    <div
      className="hello position-fixed w-50 shadow-background pt-0 d-flex justify-content-center flex-column text-white "
      style={{ zIndex: 1000, marginLeft: "220px", background: "white" }}
    >
      <IoIosCloseCircleOutline
        onClick={() => toggleAdd(false)}
        className="position-absolute top-0 left-0 text-danger fs-4 m-2 "
      />
      <h3 className="text-center fw-bold text-white">Create Room Form</h3>
      <div className="hello w-100 ">
        <Form
          {...formItemLayout}
          variant="filled"
          style={{
            maxWidth: 700,
            color: "white",
          }}
        >
          <Form.Item
            label="Title"
            rules={[
              {
                required: true,
                message: "Please input!",
              },
            ]}
            style={{ color: "white" }}
          >
            <Input
              defaultValue={title}
              onChange={(e) => {
                handleInputChange("title", e.target.value);
              }}
            />
          </Form.Item>
          <Form.Item
            label="Sub_Tiltle"
            name="Sub_Tiltle"
            rules={[
              {
                required: true,
                message: "Please input!",
              },
            ]}
          >
            <Input
              defaultValue={sub_title}
              onChange={(e) => handleInputChange("sub_title", e.target.value)}
            />
          </Form.Item>
          <Form.Item
            label="Description"
            name="TextArea"
            rules={[
              {
                required: true,
                message: "Please input!",
              },
            ]}
          >
            <Input.TextArea
              defaultValue={description}
              onChange={(e) => handleInputChange("description", e.target.value)}
            />
          </Form.Item>
          <Form.Item
            label="Bed"
            name="InputBed"
            rules={[
              {
                required: true,
                message: "Please input!",
              },
            ]}
          >
            <InputNumber
              style={{
                width: "100%",
              }}
              defaultValue={bed}
              onChange={(value) => handleInputChange("bed", value.toString())}
            />
          </Form.Item>
          <Form.Item
            label="Adult"
            name="InputAdult"
            rules={[
              {
                required: true,
                message: "Please input!",
              },
            ]}
          >
            <InputNumber
              style={{
                width: "100%",
              }}
              defaultValue={adult}
              onChange={(value) => handleInputChange("adult", value.toString())}
            />
          </Form.Item>
          <Form.Item
            label="Children"
            name="InputChildren"
            rules={[
              {
                required: true,
                message: "Please input!",
              },
            ]}
          >
            <InputNumber
              style={{
                width: "100%",
              }}
              defaultValue={children}
              onChange={(value) =>
                handleInputChange("children", value.toString())
              }
            />
          </Form.Item>
          <Form.Item
            label="Price"
            name="Mentions"
            rules={[
              {
                required: true,
                message: "Please input!",
              },
            ]}
          >
            <Mentions
              defaultValue={price}
              onChange={(value) => handleInputChange("price", parseInt(value))}
            />
          </Form.Item>
          <Form.Item
            label="Amenity"
            name="hello"
            rules={[
              {
                required: true,
                message: "Please input!",
              },
            ]}
          >
            <Mentions
              defaultValue={amenity}
              onChange={(value) =>
                handleInputChange("amenity", value.toString())
              }
            />
          </Form.Item>
          <Form.Item
            wrapperCol={{
              offset: 6,
              span: 16,
            }}
          >
            <Button onClick={handleSubmit} type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

export default FormCreate;
