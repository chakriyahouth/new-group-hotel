import { combineReducers } from "redux";
import usersSlice from "../modules/users/core/reducer";
import rolesSlice from "../modules/roles/core/reducer";
import roomTypeSlice from "../modules/roomtype/core/reducer";


export const rootReducers = combineReducers({
    users: usersSlice,
    roles: rolesSlice,
    roomtype: roomTypeSlice,
})